﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args){
      
    		
       double zuZahlenderBetragGesamt;
       double rückgabebetrag;
       boolean schleife;
       
       do {
       zuZahlenderBetragGesamt = fahrkartenbestellungErfassen(); 
       rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetragGesamt);
       fahrkartenAusgeben();
       rueckgeldAusgeben(rückgabebetrag);
       schleife = weitereFahrscheine();
       
       } while(schleife == true);
   
    }

       // Erfassung Auftrag	
	   public static double fahrkartenbestellungErfassen() {
	    

		   //Arrays ermöglichen das einfache Hinzufügen, Ändern und Löschen von Auswahlmöglichkeiten
		   //und sind somit effizienter als switch-cases für Tickettypen.
		   
		   String[] ticketart = new String[10];
		   
		   ticketart[0] = "Einzelfahrschein Berlin";
		   ticketart[1] = "Einzelfahrschein Berlin BC";
		   ticketart[2] = "Einzelfahrschein Berlin ABC";
		   ticketart[3] = "Kurzstrecke";
		   ticketart[4] = "Tageskarte Berlin AB";
		   ticketart[5] = "Tageskarte Berlin BC";
		   ticketart[6] = "Tageskarte Berlin ABC";
		   ticketart[7] = "Kleingruppen-Tageskarte Berlin AB";
		   ticketart[8] = "Kleingruppen-Tageskarte Berlin BC";
		   ticketart[9] = "Kleingruppen-Tageskarte Berlin ABC";
		   
		   double[] zuZahlenderBetrag = new double[10];
		   
		   zuZahlenderBetrag[0] = 2.90;
		   zuZahlenderBetrag[1] = 3.30;
		   zuZahlenderBetrag[2] = 3.60;
		   zuZahlenderBetrag[3] = 1.90;
		   zuZahlenderBetrag[4] = 8.60;
		   zuZahlenderBetrag[5] = 9.00;
		   zuZahlenderBetrag[6] = 9.60;
		   zuZahlenderBetrag[7] = 23.50;
		   zuZahlenderBetrag[8] = 24.30;
		   zuZahlenderBetrag[9] = 24.90;


		   for(int i=0; i < ticketart.length; i++) {

		   		System.out.printf("[" + (i+1) + "] " + ticketart[i] + ": " + "%.2f" + "€\n", zuZahlenderBetrag[i]);	
		   };
		   
		   //Nachteil dieser Ausgabe ist, dass exra ein Array für die Tickettypen angelegt werden muss.
		   //Vorteil ist, dass bei Änderung nur in den Arrays der Tickettyp und der Preis angepasst oder hinzugefügt werden muss,
		   // der Rest geschieht automatisch.
		   
		   System.out.println("\nWählen Sie ihre Wunschfahrkarte aus:\n");

		   Scanner tastatur = new Scanner(System.in);
		   
	       double ticketpreis = zuZahlenderBetrag[tastatur.nextShort()-1];
	       
	       System.out.print("Anzahl der Fahrkarten: ");
	       short anzahlFahrkarten = tastatur.nextShort();
	    
	       
	       double zuZahlenderBetragGesamt = ticketpreis * anzahlFahrkarten;
	       return zuZahlenderBetragGesamt;
	   } 
    
    
       // Geldeinwurf
       // -----------
	    
	   public static double fahrkartenBezahlen(double zuZahlenderBetragGesamt) {
	   
	    	Scanner tastatur = new Scanner(System.in);

		   
		   double eingezahlterGesamtbetrag = 0.0;
	       while(eingezahlterGesamtbetrag < zuZahlenderBetragGesamt)
	       
	       {
	    	   System.out.printf("Noch zu zahlen: %.2f Euro\n", (zuZahlenderBetragGesamt - eingezahlterGesamtbetrag));
	    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	    	   double eingeworfeneMünze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneMünze;
	       }
           
	       double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetragGesamt;
           return rückgabebetrag;

	   }

       // Fahrscheinausgabe
       // -----------------
	   public static void fahrkartenAusgeben() {
	   
	       System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");
	   }
       

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
	   
	   public static void rueckgeldAusgeben(double rückgabebetrag) {
	   
	       if(rückgabebetrag > 0.0)
	       {
	    	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro\n", rückgabebetrag);
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");
	
	           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
	           {
	        	  System.out.println("2 EURO");
		          rückgabebetrag -= 2.0;
	           }
	           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	           {
	        	  System.out.println("1 EURO");
		          rückgabebetrag -= 1.0;
	           }
	           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
	           {
	        	  System.out.println("50 CENT");
		          rückgabebetrag -= 0.5;
	           }
	           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
	           {
	        	  System.out.println("20 CENT");
	 	          rückgabebetrag -= 0.2;
	           }
	           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
	           {
	        	  System.out.println("10 CENT");
		          rückgabebetrag -= 0.1;
	           }
	           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
	           {
	        	  System.out.println("5 CENT");
	 	          rückgabebetrag -= 0.05;
	           }
	       }
	

	    }
	   
	   public static boolean weitereFahrscheine() {
		   
		   System.out.println("\nMöchten Sie weitere Fahrscheine kaufen? (j/n)\n");
		   Scanner tastatur = new Scanner(System.in);
		   
		   char fahrscheine = tastatur.next().charAt(0);
		   boolean janein;
		   if (fahrscheine == 'j') {
			   janein = true;
		   }
		   else if (fahrscheine == 'n') {
			   janein = false;
		       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                       "vor Fahrtantritt entwerten zu lassen!\n"+
                       "Wir wünschen Ihnen eine gute Fahrt.");
		   }
		   else {
			   System.out.println("\nFehler!\n");
			   janein = false;
		       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                       "vor Fahrtantritt entwerten zu lassen!\n"+
                       "Wir wünschen Ihnen eine gute Fahrt.");
			   	   
		  }
		   return janein;
	   }
	   
}

		// Die Anzahl der Fahrkarten wurde als short definiert, da nur eine ganzzahlige 
		// Anzahl an Fahrkarten logisch Sinn ergibt. Anschließend wurden alle zuZahlenderBetrag durch
		// zuZahlenderBetragGesamt ergänzt


import java.util.Scanner;

public class Quadrat {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Geben Sie die Seitenlänge des Quadrates ein, das ausgegeben werden soll:");
		int laenge = tastatur.nextInt();
		
		for(int i = 1; i <= laenge; i++) {
		
			System.out.print("*");		
		}
		
		System.out.println();
		
		for(int i = 1; i <= (laenge-2); i++) {
			System.out.print("*");
			for(int j = 1; j <= (laenge-2); j++) {
				System.out.print(" ");
			}
		    System.out.println("*");
		}
		
		for(int i = 1; i <= laenge; i++) {
			
			System.out.print("*");		
		}			
		
	}

}

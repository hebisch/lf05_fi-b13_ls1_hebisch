import java.util.Scanner;

public class OhmschesGesetz {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Welche Größe möchten Sie errechnen? (R/U/I)");
		char variable = tastatur.next().charAt(0);
		
		switch (variable) {
		
		case 'r':
			System.out.println("Geben Sie die Spannung (U) in Volt ein");
			double u = tastatur.nextDouble();
			
			System.out.println("Geben Sie die Stromstärke (I) in Ampere ein");
			double i = tastatur.nextDouble();
			
			System.out.println("Der Widerstand (R) beträgt " + (u/i) + " Ohm.");
			break;
			
		case 'u':	
			System.out.println("Geben Sie die Widerstand (R) in Ohm ein");
			double r = tastatur.nextDouble();
			
			System.out.println("Geben Sie die Stromstärke (I) in Ampere ein");
			i = tastatur.nextDouble();
			
			System.out.println("Die Spannung (U) beträgt " + (r*i) + " Volt.");
			break;
	
		case 'i':
			System.out.println("Geben Sie die Spannung (U) in Volt ein");
			u = tastatur.nextDouble();
			
			System.out.println("Geben Sie den Widerstand (R) in Ohm ein");
			r = tastatur.nextDouble();
			
			System.out.println("Die Stromstärke (A) beträgt " + (u/r) + " Ampere.");
			break;	
			
		default:
			System.out.println("Fehler!");
			
		
		}		
		
	}

}

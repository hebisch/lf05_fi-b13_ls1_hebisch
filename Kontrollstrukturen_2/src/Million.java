import java.util.Scanner;

public class Million {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	boolean nochmal = false;
	
	do {
			
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Willkommen beim Millionärsrechner!\n");
		
		System.out.println("Geben Sie die Höhe Ihrer Einlage in Euro ein");
		double geld = tastatur.nextDouble();
		
		System.out.println("Geben Sie Ihren Zinssatz in Prozent ein");
		double zinssatz = (1+ (tastatur.nextDouble()/100));
		
		int i = 0;
	
		
		if(zinssatz < 1 && geld < 1000000) {
			
			System.out.println("Mit Negativzinsen werden Sie nie Millionär!");
		}
		
		else if(zinssatz < 1 && geld > 1000000) {
			
			System.out.println("Sie sind bereits Millionär, bei negativen Zinsen jedoch bald nicht mehr!");
			
		}	
			
		else if(zinssatz >= 1 && geld > 1000000) {
			
			System.out.println("Sie sind bereits Millionär!");
		}
			
		
		else {
		while(geld < 1000000) {
			
		geld = geld * zinssatz;
		
		i++;
			
		}
		
		if(i != 1) {
		System.out.println("Sie müssen " + i + " Jahre warten, um Millionär zu werden");
		}
		
		else {
		System.out.println("Sie müssen " + i + " Jahr warten, um Millionär zu werden!");
		
		}
		}
		
		System.out.println("\nMöchten Sie eine weitere Berechnung durchfüren? (j/n)");
		char abfrage = tastatur.next().charAt(0);
	
		switch(abfrage) {
		
			case 'j':
				nochmal = true;
				break;
				
			case 'n':
				System.out.println("Abfrage beendet.");
				nochmal = false;
				break;
				
			default:
				System.out.println("Fehler!");
				nochmal = false;
		
		}
	} while (nochmal == true);	

	}

}

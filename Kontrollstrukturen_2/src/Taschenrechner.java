import java.util.Scanner;

public class Taschenrechner {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Geben Sie eine Zahl ein.");	
		double erstezahl = tastatur.nextDouble();
		
		System.out.println("Geben Sie eine weitere Zahl ein.");
		double zweitezahl = tastatur.nextDouble();
		
		System.out.println("Geben Sie einen Rechenoperator (+, -, *, /) ein.");
		char c = tastatur.next().charAt(0);
		
		switch (c) {
		
		case '+':
			System.out.println("Das Ergebnis ist " + (erstezahl+zweitezahl));
			break;
		
		case '-':
			System.out.println("Das Ergebnis ist " + (erstezahl-zweitezahl));
			break;
		
		case '*':
			System.out.println("Das Ergebnis ist " + (erstezahl*zweitezahl));
			break;
			
		case '/':
			System.out.println("Das Ergebnis ist " + (erstezahl/zweitezahl));
			break;
		
		default:
			System.out.println("Fehler!");
		
		}
	}	
		
}

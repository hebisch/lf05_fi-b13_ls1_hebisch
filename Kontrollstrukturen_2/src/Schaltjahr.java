import java.util.Scanner;

public class Schaltjahr {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

			
		Scanner tastatur = new Scanner(System.in);

		System.out.println("Geben Sie eine Jahreszahl ein");
		int jahreszahl = tastatur.nextInt();
		
		if (jahreszahl % 4 == 0 && jahreszahl < 1582) {
			System.out.println(jahreszahl + " ist ein Schaltjahr");
		}
		
		else if (jahreszahl % 4 == 0 && jahreszahl % 100 != 0 && jahreszahl >= 1582) {
			System.out.println(jahreszahl + " ist ein Schaltjahr");
		}
		
		else if (jahreszahl % 400 == 0 && jahreszahl >= 1582) {
			System.out.println(jahreszahl + " ist ein Schaltjahr");
		}
		
		else {
			System.out.println(jahreszahl + " ist kein Schaltjahr");
		}
				
		
	}

}
